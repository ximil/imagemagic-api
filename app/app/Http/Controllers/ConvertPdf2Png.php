<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ConvertPdf2Png extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        /* Get local disk instance */
        $local_disk = Storage::disk('local');
        
        /* Set output image width */
        $image_width = 500;
        if($request->has('width')){
            if(is_numeric($request->width)){
                $image_width = $request->width;
            }
        }

        /* Save given file */
        $path = 'file not received';
        if(!$request->has('file')){
            return response()->json('No se recibió el archivo',400);    
        }

        \Log::debug('request file field',[$image_width]);
        
        try{
            $path = $request->file->store('convert');
        }
        catch(\Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException $e){
            \Log::error('Error al guardar el archivo',[$e->getMessage()]);
            \Log::error('Falló el proceso de carga del archivo ',[$request->file('file')->getClientOriginalName(),$path]);
            return response()->json('Archivo inválido',415);
        }
        
        \Log::info('Cargado el archivo',[$request->file('file')->getClientOriginalName(),$path]);
    
        $process = new Process('convert -density 150 -antialias '. pathinfo($path, PATHINFO_BASENAME) . ' -append -resize '.$image_width.'x -quality 100 ' . pathinfo($path, PATHINFO_FILENAME).'.png');
        $process->setWorkingDirectory('/var/www/html/storage/app/convert');
        $process->setTimeout(3600);
		$process->run();
		if (!$process->isSuccessful()) {
		    \Log::error('Falló el proceso de conversión del archivo ',[$request->file('file')->getClientOriginalName(),$path]);
		    return response()->json('Falló el proceso de conversión',500);
		}
    	
    	\Log::info('Finalizó el proceso de conversión del archivo ',[$request->file('file')->getClientOriginalName(),$path]);

    	if(!file_exists(storage_path().'/app/convert/'.pathinfo($path, PATHINFO_FILENAME).'.png')){
    	    sleep(5);
    	}
    	
    	$headers = array(
            'Content-Type' => 'image/png'
        );
    
        return response()->file(storage_path().'/app/convert/'.pathinfo($path, PATHINFO_FILENAME).'.png',$headers)->deleteFileAfterSend();
        
    }
}
