<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* this function receive parameters as form-data including name, and file (tiff file stream) */
Route::post('convert/tiff2pdf', 'ConvertTiff2Pdf');

/* this function receive parameters as form-data including name, width and file (pdf file stream) */
Route::post('convert/pdf2png', 'ConvertPdf2Png');

Route::get('status', function(){
    return response()->json('ok',200);
});