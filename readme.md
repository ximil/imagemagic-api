**ximil/imagemagic-api**  
Image to convert files between different formats by using imagemagick library.  
Laravel 5.7 based Rest API to allow conversions
    
**Convert tiff to pdf**  
method: post  
url: public/convert/tiff2pdf  
multipart header needed  
params:   
* file (bytes)  
  
**Convert pdf to png**  
method: post  
url: public/convert/pdf2png  
multipart header needed  
params:   
* file (bytes)  
* width: number for png resulting width in pixels